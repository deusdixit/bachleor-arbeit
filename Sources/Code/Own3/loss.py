import matplotlib.pyplot as plt
import torch
import torch.nn as nn
from pytorch3d.loss import chamfer_distance
from torch.nn.utils.rnn import pad_sequence


class SILogLoss(nn.Module):  # Main loss function used in AdaBins paper
    def __init__(self):
        super(SILogLoss, self).__init__()
        self.name = 'SILog'

    def forward(self, pred, target, edge, mask=None, interpolate=True):
        if interpolate:
            pred = nn.functional.interpolate(pred, target.shape[-2:], mode='bilinear', align_corners=True)
        # p = target.cpu().detach().numpy()[0][0]
        # e = edge.cpu().detach().numpy()[0][0]
        # e = (e - 1.0) * 255.0
        # print("Bild shape : " + str(p.shape) + " - Kantenbild shape : " + str(e.shape))
        # f = plt.figure()
        # f.add_subplot(1,2, 1)
        # plt.imshow(p,cmap='plasma')
        # f.add_subplot(1,2, 2)
        # plt.imshow(e,cmap='gray', vmin=0, vmax=255)
        # plt.show(block=True)

        # print(edge) 
        if mask is not None:
            pred = pred[mask]
            target = target[mask]
            edge = edge[mask]
            
        g = torch.log(pred) - torch.log(target)
        g = g * edge
        #w = (edge / 255.0) + 1.0
        #g = g * w
        #print(w)
        #print(edge)
        # n, c, h, w = g.shape
        # norm = 1/(h*w)
        # Dg = norm * torch.sum(g**2) - (0.85/(norm**2)) * (torch.sum(g))**2

        Dg = torch.var(g) + 0.15 * torch.pow(torch.mean(g), 2)
        result = 10 * torch.sqrt(Dg)
        return result


class BinsChamferLoss(nn.Module):  # Bin centers regularizer used in AdaBins paper
    def __init__(self):
        super().__init__()
        self.name = "ChamferLoss"

    def forward(self, bins, target_depth_maps):
        bin_centers = 0.5 * (bins[:, 1:] + bins[:, :-1])
        n, p = bin_centers.shape
        input_points = bin_centers.view(n, p, 1)  # .shape = n, p, 1
        # n, c, h, w = target_depth_maps.shape

        target_points = target_depth_maps.flatten(1)  # n, hwc
        mask = target_points.ge(1e-3)  # only valid ground truth points
        target_points = [p[m] for p, m in zip(target_points, mask)]
        target_lengths = torch.Tensor([len(t) for t in target_points]).long().to(target_depth_maps.device)
        target_points = pad_sequence(target_points, batch_first=True).unsqueeze(2)  # .shape = n, T, 1

        loss, _ = chamfer_distance(x=input_points, y=target_points, y_lengths=target_lengths)
        return loss
