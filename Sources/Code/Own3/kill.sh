#!/bin/bash

ps -aux | grep ada/bin | tr -s " " | cut -d " " -f 2 | while read a;
do
    kill -9 $a
done
